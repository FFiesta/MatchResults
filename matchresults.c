#include <stdio.h>
#include <string.h>

//--> WARNING: STRING ARRAY KNOWLEDGE REQUIRED. <--//

int main()
{
	const int chlimit = 11; // Character limit
	const int pqty = 4; // Player quantity
	const int stype = 2; // Score type (0 = kills, 1 = deaths)
	
	/* char player0[chlimit];
	char player1[chlimit];
	char player2[chlimit];
	char player3[chlimit]; */

	char player[chlimit][pqty];

	int playerScore[pqty][stype];
	int playerID[pqty];
	int i;
	
	/* strcpy(player0, "Arklite");
	strcpy(player1, "General Lee");
	strcpy(player2, "Fabbot");
	strcpy(player3, "Natss");*/

	strcpy(player[0], "Arkl1te");
	strcpy(player[1], "General_Lee");
	strcpy(player[2], "Fabbot");
	strcpy(player[3], "Natss");

	// Player 0
	playerScore[0][0] = 16;	// K
	playerScore[0][1] = 7;	// D
	// Player 1
	playerScore[1][0] = 8;
	playerScore[1][1] = 8;
	// Player 2
	playerScore[2][0] = 12;
	playerScore[2][1] = 13;
	// Player 3
	playerScore[3][0] = 21;
	playerScore[3][1] = 5;
	
	
	
	for(i = 0; i < pqty; i++){
		printf("%s\t| %d | %d\n", player[i], playerScore[i][0], playerScore[i][1]);
	}

	return 0;
}